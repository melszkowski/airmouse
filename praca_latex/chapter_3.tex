\chapter{Opis teoretyczny elektronicznych czujników inercyjnych}
Do badania położenia i~orientacji w~przestrzeni używa się najczęściej trzech rodzajów czujników inercyjnych. Są to akcelerometr, żyroskop i~magnetometr (powszechnie zwany cyfrowym kompasem). Służą one kolejno do mierzenia przyspieszeń ciała, prędkości kątowych ciała oraz do wyznaczania kierunku ziemskiego pola magnetycznego.
Czujniki inercyjne można podzielić na różne rodzaje w~zależności od sposobu działania i~procesu produkcji, a~każdy ma swoje cechy i~indywidualne zastosowania. Ze względu na potrzebną miniaturyzację, optymalizację kosztów, trwałość, a~także oszczędność energii, stosuje się dziś układy z~jak najmniejszą liczbą tradycyjnych elementów ruchomych, takie jak MEMS, czyli mikroukłady elektromechaniczne.
\section{MEMS -~wprowadzenie}
Szybkiemu postępowi technologicznemu i~idącej wraz z~nim miniaturyzacji zawdzięczamy nie tylko mikroskopowej skali układy elektroniczne, ale również mikroskopowe urządzenia mechaniczne. Zasilane siłami elektrostatycznymi, magnetycznymi, elektromagnetycznymi, pneumatycznymi czy cieplnymi są to wszelakie możliwe wynalazki znane nam od wielu lat: silniki, zawory, koła zębate, membrany czy pincety, wszystkie o~rozmiarach mniejszych niż 100 mikrometrów. Wraz z~równoczesnym wykorzystaniem elementów elektronicznych, takich jak kondensatory i~tranzystory, możliwe stało się wykonanie mikrourządzeń elektromechanicznych, zwanych powszechnie MEMS (ang. \textit{microelectromechanical system}). Ich charakterystycznymi cechami jest “integracja (scalenie) na jednym podłożu (najczęściej krzemowym) elementów mech. lub mikromaszyn z~układami scalonymi'' \cite{mems_encyklopedia} oraz rozmiary “od pojedynczych cm do pojedynczych mikrometrów'' \cite{mems_encyklopedia}. Definicja ta nie jest jednak spójna i~niektórzy podają konkretne rozmiary, określając MEMS np. jako “urządzenia o~charakterystycznej długości mniejszej niż 1 mm, lecz większej niż 1 mikrometr'' \cite{mems_introduction_and_fundamentals}. Układy te są powszechnie używane od wielu lat i~ich rynek nadal się dynamicznie rozwija. Ze względu na coraz mniejsze rozmiary i~nowe procesy produkcji wpływające na mniejsze koszta, mikroukłady elektromechaniczne stosowane są przykładowo w: \begin{itemize}
\item branży motoryzacyjnej -~czujniki przeciążeń aktywujące poduszki powietrzne, wyrównywanie toru jazdy
\item fotografii -~optyczna stabilizacja obrazu,
\item lotnictwie -~stabilizacja pozycji bezzałogowych statków powietrznych,
\item rozrywce -~kontrolery gier, wirtualna rzeczywistość,
\item systemach dźwiękowych -~aktywne głośniki niskotonowe, mikrofony,
\item dyskach twardych, zabezpieczając by w~trakcie upadku głowica nie uderzyła o~talerze.
\end{itemize} 
MEMS ze względu na sposób działania można podzielić na dwie grupy: aktuatory (elementy wykonawcze) oraz sensory. Obie grupy można sklasyfikować jako przetworniki -~ich wspólnym zadaniem jest zamiana jednego rodzaju energii na drugi, tzn. energię elektryczną na mechaniczną i~odwrotnie. Spotkać można też systemy wykorzystujące procesy chemiczne, które zwane są mikroreaktorami chemicznymi.

\section{Sposób działania sensorów MEMS}
Czujnik definiuje się jako ``urządzenie zdolne do wychwytywania zmian pewnej wielkości fizycznej, pomiaru wartości tejże albo sygnalizujące pojawienie się pewnego bodźca, sygnału'' \cite{czujnik_definicja}. Sensorami w~technologii MEMS można badać różne wielkości fizyczne, takie jak prąd, przyspieszenie liniowe, kątowe, temperaturę, ciśnienie i~wiele innych. Budowa i~sposób działania zależy od natury badanego zjawiska.

\subsection{Akcelerometry}
Podstawową ideą akcelerometrów jest konwertowanie ruchu niejednostajnego na zmiany odchylenia lub nacisku. Te zmiany następnie są transformowane na wektor sygnału elektrycznego, który w~przybliżeniu reprezentuje wektor przyspieszenia działający na czujnik \cite{inertial_mems_principles_and_practice}. Z~ogólnego punktu widzenia, akcelerometr powinien zwracać informację o~przyspieszeniu we wszystkich trzech osiach, jednak produkuje się również czujniki działające w~jednym lub dwóch wymiarach, jednak na układ stale działają siły o~każdym kierunku. W~takiej sytuacji potrzebne jest zminimalizowanie działania na czujnik sił o~niepożądanych wektorach. Schemat widoczny na rysunku \ref{fig:accel-oned} pokazuje uproszczoną budowę akcelerometru MEMS.
\begin{figure}[h]
	\center
	\includegraphics[scale=0.7]{img/akcel_schemat.png}
	\caption{\textit{Uproszczony schemat budowy akcelerometru liniowego, jednowymiarowego. Opracowano na podstawie \cite{budowa_accel}}}
	\label{fig:accel-oned}
\end{figure}
Zawieszona na sprężynach masa oscyluje pod wpływem sił zewnętrznych. Grzebieniowa struktura na boku masy porusza się między inną grzebieniową strukturą umieszczoną na wewnętrznej ścianie układu, co dzięki zmianom odległości między pojedynczymi kolcami struktur w~praktyce tworzy kondensatory o~zmiennej pojemności, w~których wartość ta maleje proporcjonalnie do wzrostu odległości zgodnie ze wzorem \cite{podstawy_fizyki}:
$$
C =~ \frac{\varepsilon _{0} \varepsilon _{r} S}{d}
$$
gdzie: d~– odległość między okładkami, a~$\varepsilon _{0}$ -~przenikalność elektryczna próżni, $\varepsilon _{r}$ -~względna przenikalność elektryczna ośrodka, z~którego wykonano dzielący okładki izolator, S~- powierzchnia jednej okładki kondensatora.
\\
\paragraph{}Przyspieszenia działając na zawieszoną masę zmieniają pojemność kondensatorów, która reprezentuje zmianę ruchu w~jednym kierunku. Dla pozostałych dwóch wymiarów schemat i~obliczenia są analogiczne.
Struktury grzebieniowe mogą być również umieszczone na promieniach odchodzących od punktu centralnego, jak pokazuje rysunek \ref{fig:accel-angular}.

\begin{figure}[h]
	\center
	\includegraphics[scale=1.0]{img/akcel_katowy.png}
	\caption{\textit{Rozmieszczenie kondensatorów w~akcelerometrze kątowym. Opracowano na podstawie \cite{grzebien_katowy}}}
	\label{fig:accel-angular}
\end{figure}

Takie rozmieszczenie sprawia, że bezwładna masa umieszczona na środku będzie wrażliwa na ruch obrotowy zamiast liniowego, co pozwala mierzyć przyspieszenia kątowe.

 \subsection{Żyroskopy}
Same żyroskopy można podzielić na dwie główne grupy: kierunkowe i~prędkościowe, choć w~technologii MEMS najczęściej spotykane są prędkościowe \cite{mems_uzytkowe}. Żyroskop prędkościowy mierzy swoją prędkość kątową w~kartezjańskim układzie współrzędnych w~jednym, w~dwóch lub w~trzech wymiarach równocześnie, w~zależności od specyfikacji, tak jak ma to miejsce w~akcelerometrach. Żyroskopy wibracyjne, czyli takie jakie są produkowane w~technologii MEMS wykorzystują skutki wynikające z~efektu Coriolisa. Zjawisko to polega na tym, że dla obserwatora nieruchomego względem obracającego się nieinercjalnego układu odniesienia ciało które się w~tym układzie porusza ruchem prostoliniowym, będzie wydawać się ruchem zakrzywionym, co pokazuje rysunek \ref{fig:coriolis}.
\begin{figure}[h]
	\center
	\includegraphics[scale=1.0]{img/coriolis.png}
	\caption{\textit{Skutki efektu Coriolisa z~punktu widzenia obserwatora. Opracowano na podstawie \cite{coriolis_image}}}
	\label{fig:coriolis}
\end{figure}
W konstrukcji żyroskopów również wykorzystywane jest ciało o~określonej masie zawieszone na sprężynach. Układ ten różni się jednak tym, że masa umieszczona jest na obrotowej platformie i~wprowadzona w~drgania. ``(...)  gdy masa przesuwa się w~kierunku środka tarczy, wywiera siłę o~zwrocie w~prawo. By możliwy był pomiar przyspieszenia Coriolisa, ramę z~drgającą masą przymocowuje się sprężynami do podłoża pod kątem prostym do kierunku przemieszczania się masy'' \cite{mems_uzytkowe}. Zmiana na sygnał elektryczny następuje identycznie jak w~przypadku akcelerometru -~za pomocą mierzenia pojemności kondensatorów o~grzebieniowej strukturze cechujących się zmiennymi odległościami między ich płytkami.
Ze względu na sposób budowy żyroskopów MEMS można wyróżnić żyroskopy kamertonowe (ang. \textit{tuning fork}), żyroskopy z~wibrującym kołem lub wykorzystujące inne rezonujące ciało stałe. Ogólna zasada działania jest jednak identyczna.
Ze względu na to, że żyroskopy mierzą prędkość kątową, do wyznaczenia bezwzględnego kąta obrotu potrzebne jest scałkowanie pomiaru. Niedokładność samego czujnika spowodowana szumem otoczenia oraz niedoskonałościami wykonania sprawia, że odczyty nie są idealne i~wprowadzają błędy, które kumulują się w~procesie całkowania. Zjawisko to nazywa się dryftem żyroskopu i~oznacza, że niemożliwe jest precyzyjne określenie tylko za pomocą żyroskopu różnicy kąta obrotu danego ciała w~czasie. By zminimalizować ten problem, stosuje się fuzję dwóch czujników, przykładowo łączy się dane z~trój-osiowego akcelerometru oraz trój-osiowego żyroskopu lub często też trój-osiowego magnetometru. 

\section{Sposób interakcji z~czujnikami elektronicznymi z~poziomu mikrokontrolera}
Czujniki mogą przesyłać zebrane dane na dwa sposoby: analogowo lub cyfrowo. Czujniki analogowe są znacznie tańsze i~proste w~konstrukcji, a~ich sposób działania opiera się na przekształcaniu mierzonej wielkości fizycznej (np. temperatury lub intensywności światła) na sygnał elektryczny o~określonym napięciu albo zmieniając wewnętrzną rezystancję. Zmiana nie zawsze odbywa się w~sposób liniowy, w~zależności od zastosowań przekształcenie może mieć również charakter logarytmiczny lub wykładniczy. Przykładami takich urządzeń są potencjometry, termistory, fotorezystory etc.
\\
Innym sposobem przesyłania jest znacznie bardziej złożone, oparte na interfejsach cyfrowych. W~tym wypadku czujniki mają zintegrowane w~sobie przetworniki analogowo-cyfrowe i~są zdolne do wysyłania informacji w~postaci ciągu bitów. W~elektronice istnieje kilka standardów komunikacji przewodowej, do których należą między innymi:
\begin{itemize}
\item I\textsuperscript{2}C (znane również jako IIC) -~szeregowa magistrala, w~której przesył sygnału odbywa się w~sposób synchroniczny. Do działania wymaga dwóch linii, określanych jako SDA -~linia danych oraz SCL -~linia sygnału zegarowego. W~tego typu magistrali każde urządzenie ma przypisany swój adres, który jest liczbą 10-bitową, co w~teorii pozwala na podłączenie 1023 urządzeń (adres 0 jest zarezerwowanym adresem rozgłoszeniowym). Magistrala działa w~architekturze master-slave, choć możliwe jest też  podłączenie w~trybie multi-master.
\item SPI -~podobnie jak I\textsuperscript{2}C, SPI działa w~sposób szeregowy, przesyłając dane synchronicznie. Do komunikacji wymagane są 4 linie, najczęściej określane jako SCLK (sygnał zegarowy), MOSI (ang. \textit{Master Output Slave Input}, czyli dane przesyłane od węzła master), MISO (ang. \textit{Master Input Slave Output}, czyli dane przesyłane od węzłów slave) oraz SS (ang. \textit{Slave select}, wybór urządzenie które w~danej chwili ma odbierać dane).
\item UART -~jest jednym z~najprostszych sposobów przesyłania danych, w~którym dokładnie dwa urządzenia są podłączone do siebie dwoma przewodami, wyjście urządzenia A~jest podłączone do wejścia urządzenia B~i odwrotnie. Transmisja odbywa się asynchronicznie, a~prędkość musi być zgodna i~ustalona między komunikującymi się obiektami.
\end{itemize} 

\section{Wybrane sposoby fuzji danych z~akcelerometrów i~żyroskopów}
Elektroniczne czujniki inercyjne są obarczone niedokładnościami. Należą do nich błędy, które dotyczą każdego przyrządu pomiarowego mierzącego określoną wartość fizyczną. Jest to na przykład niepewność pomiarowa wynikająca z~dokonywania pomiaru ze skończoną dokładnością \cite{prezentacja_pomiary}. Sensory MEMS mimo dużego stopnia zaawansowania technologicznego wprowadzają dodatkowe nieprawidłowości. Są one podatne na sygnały elektromagnetyczne oraz zmiany temperatury. Ponadto akcelerometry są wrażliwe na ruchy liniowe, w~związku z~czym wibracje otoczenia wprowadzają do pomiarów szum \cite{fusion_method}. Żyroskopy z~kolei mają tendencję do narastającego błędu pomiaru w~związku z~brakiem jednoznacznego, stałego punktu odniesienia oraz kumulatywnego błędu wynikającego z~całkowania numerycznego, które potrzebne jest do określenia pozycji na podstawie prędkości kątowej. W~praktyce by zminimalizować niedoskonałości stosuje się równocześnie dane z~dwóch lub trzech czujników, co w~konsekwencji pozwala na uzyskanie stabilniejszych wyników.
Istnieją różne algorytmy zajmujące się fuzją danych z~czujników. Jednym z~najpopularniejszych i~używanych powszechnie w~nawigacji, namierzaniu i~transporcie jest filtr Kalmana. Filtr ten składa się z~dwóch faz, wykonywanych naprzemiennie. Pierwsza jest faza predykcji stanu, która szacuje kolejny stan opisywanego procesu. Druga faza, zwana fazą korekcji wykorzystuje aktualne dane pomiarowe by poprawić wcześniejszą estymatę \cite{filtry_pwr}. Innym popularnym algorytmem jest filtr komplementarny. Filtr ten jest prostszy of filtru Kalmana, a~jego zastosowanie ogranicza się do sytuacji gdy łączymy dwa sygnały, gdzie jeden cechuje się wysokim poziomem szumu w~wysokich częstotliwościach, a~drugi szumem w~niskich częstotliwościach \cite{complementary_vs_kalman}. Oba sygnały są filtrowane (jeden przez filtr górnoprzepustowy, drugi dolnoprzepustowy), a~następnie są sumowane \cite{metody_wyznaczania_katow}. Do połączenia danych z~trzech czujników, takich jak akcelerometr, żyroskop i~magnetometr potrzebne są bardziej złożone metody, takie jak filtr Mahony'ego, lub nowszy, inspirowany nim filtr Madgwicka \cite{madgwick_paper}. 


