\ProvidesClass{praca_inzynierska} 
\LoadClass[a4paper,12pt,titlepage]{report}
%Dołączenie paczek
\usepackage[polish]{babel} %włączenie paczki językowej babel
\usepackage[utf8]{inputenc} %ustawienie kodowania na utf8
\usepackage[T1]{fontenc} 
\usepackage{graphicx} %włączenie biblioteki do wczytywania obiektów graficznych
\usepackage{hyperref} %włączenie biblioteki umożliwiającej działanie na hyperlinkach
\usepackage{color} %włączenie biblioteki do kolorowania tekstu
\usepackage{graphics} %włączenie biblioteki przetwarzającej obrazy
\usepackage{listings}
\usepackage{fancyhdr} %do wykonania stopki
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{rotating}
\usepackage{pdflscape}
\usepackage{afterpage}
\usepackage{float}
\usepackage{pdfpages}
\usepackage{algorithmic}
\usepackage{algorithm}


%definicje nazw kolorów
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

%ustawienia pakietu listings (wyświetlanie kodu źródłowego
\lstset{ %
  language=C,                   % the language of the code
  basicstyle=\normalsize,       	% the size of the fonts that are used for the code
  numbers=left,                   % where to put the line-numbers
  numberstyle=\tiny\color{gray},  % the style that is used for the line-numbers
  stepnumber=1,                   % the step between two line-numbers. If it's 1, each line 
                                  % will be numbered
  frame=single,
  numbersep=5pt,                  % how far the line-numbers are from the code
  backgroundcolor=\color{white},  % choose the background color. You must add \usepackage{color}
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  tabsize=1,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  title=\lstname,                 % show the filename of files included with \lstinputlisting;
                                  % also try caption instead of title
  keywordstyle=\color{blue},      % keyword style
  commentstyle=\color{dkgreen},   % comment style
  stringstyle=\color{mauve},      % string literal style
  escapeinside={\%*}{*)},         % if you want to add LaTeX within your code
  morekeywords={*,...},           % if you want to add more keywords to the set
  belowskip=0em,			      % margin below code
}
% Zdefiniowanie autora i~tytułu:
\author{Michał Elszkowski}
\title{Sterowanie komputerem z wykorzystaniem czujników inercyjnych oraz platformy Arduino}
\hypersetup{pdfauthor={Michał Elszkowski},pdftitle={Sterowanie komputerem z wykorzystaniem czujników inercyjnych oraz platformy Arduino} }
\frenchspacing
\pagestyle{plain}