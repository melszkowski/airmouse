/* ============================================
I2Cdev device library code is placed under the MIT license
Copyright (c) 2012 Jeff Rowberg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
===============================================
*/

// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"

#include <Mouse.h>

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

#define INTERRUPT_PIN 7

#define MAX_CHAR_VAL 127

// 180 / pi
// used for rad -> deg conversion
#define CONVERSION_CONSTANT 57.2957795131;

// describes at what roll value a click event is triggered. 
// apllies to both left and right click, with 
// -1 * TILD_THRESHOLD and TILT_THRESHOLD respectively
#define TILT_THRESHOLD 30.0

// describes when readyToClick boolean is set
// readyToClick is true after roll angle X is
// -1 * CLICK_READY_THRESHOLD < X < CLICK_READY_THRESHOLD
#define CLICK_READY_THRESHOLD 20.0

// pins associated with mouse handling:
#define LED_MOUSE 13

MPU6050 mpu;

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

float rollDegrees;

// ================================================================
// == MOUSE ==
// ================================================================

// boolean indicating if mouse click is possible
// it's ready to click after coming back to more straight orientation
// it is to prevent from bouncing-like effect that is present with 
// physical buttons
bool readyToClick = false;

// how much to move cursor in X and Y axes:
short mouseX = 0;
short mouseY = 0;

// used for calculating cursor's distance to move
float lastYaw = 0.0;
float lastPitch = 0.0;
float deltaYaw = 0;
float deltaPitch = 0;

// used for increasing the sensor readings
// cursor speed depends on this
// value determined after manual testing
#define MULTIPLIER 1800

bool isFirstLoop = true;

bool isMouseEnabled = false;

void inline moveMouse(short xValue, short yValue);

// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

// indicates whether MPU interrupt pin has gone high
volatile bool mpuInterrupt = false;
void dmpDataReady() {
    mpuInterrupt = true;
}

// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

void setup() {

// -----------------
// MOUSE related
// -----------------
    Mouse.begin();
    
    pinMode(LED_MOUSE, OUTPUT);
    digitalWrite(LED_MOUSE, LOW);
    
// -----------------
// DMP
// -----------------
// join I2C bus (I2Cdev library doesn't do this automatically)
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
        Wire.setClock(400000); // 400kHz I2C clock
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    // initialize serial communication
    Serial.begin(115200);
    while (!Serial); // needed for Leonardo (enumeration)

    // initialize device
    Serial.println(F("Initializing I2C devices..."));
    mpu.initialize();
    pinMode(INTERRUPT_PIN, INPUT);

    // verify connection
    Serial.println(F("Testing device connections..."));
    Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

    // wait for ready
    Serial.println(F("\nSend any character to begin DMP programming and demo: "));
    while (Serial.available() && Serial.read()); // empty buffer
    while (!Serial.available());                 // wait for data
    while (Serial.available() && Serial.read()); // empty buffer again

    // load and configure the DMP
    Serial.println(F("Initializing DMP..."));
    devStatus = mpu.dmpInitialize();

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        Serial.println(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
        Serial.print(digitalPinToInterrupt(INTERRUPT_PIN));
        Serial.println(F(")..."));
        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        Serial.println(F("DMP ready! Waiting for first interrupt..."));
        Serial.println(F("Keep your device completely steady! Calibrating sensors...")); 
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();

        // default is 4
        // sample Rate = Gyroscope Output Rate / (1 + SMPLRT_DIV)
        // SMPLRT_DIV is the param
        mpu.setRate(3);
        
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
    }
}



// ================================================================
// ===                    MAIN PROGRAM LOOP                     ===
// ================================================================

void loop() {
    // if programming failed, don't try to do anything
    if (!dmpReady) return;

    // wait for MPU interrupt or extra packet(s) available
    while (!mpuInterrupt && fifoCount < packetSize) {
        if (mpuInterrupt && fifoCount < packetSize) {
          // try to get out of the infinite loop 
          fifoCount = mpu.getFIFOCount();
        }  
        // other program behavior stuff here
    }

    // reset interrupt flag and get INT_STATUS byte
    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    // get current FIFO count
    fifoCount = mpu.getFIFOCount();

    // check for overflow (this should never happen unless our code is too inefficient)
    if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        fifoCount = mpu.getFIFOCount();
        Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT)) {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;

        // display Euler angles in degrees
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);

        if(isFirstLoop) {
          lastYaw = ypr[0];
          lastPitch = ypr[2];
          isFirstLoop = false;
          return;
        }

        // convert rad to degrees
        // instead of doing x * 180 / pi
        // we use precomputed value from "define" macro
        rollDegrees = ypr[1] * CONVERSION_CONSTANT; 
        
        deltaYaw = ypr[0] - lastYaw;
        deltaYaw *= MULTIPLIER;

        deltaPitch = ypr[2] - lastPitch;
        deltaPitch *= MULTIPLIER;

        lastYaw = ypr[0];
        lastPitch = ypr[2];

        if (isMouseEnabled) {

          // if roll is quite low, we can be ready to click again
          if (abs(rollDegrees) < CLICK_READY_THRESHOLD) {
            readyToClick = true;
          }
          if (readyToClick) {
            if (rollDegrees > TILT_THRESHOLD) {
              Mouse.click(MOUSE_RIGHT);
              readyToClick = false;
            } else if (rollDegrees < (-1.0 * TILT_THRESHOLD)){
              Mouse.click(MOUSE_LEFT);
              readyToClick = false;
            }
          }
          
          moveMouse((int) deltaYaw, (int) deltaPitch);
          
        } else {
          // if the mouse is not enabled, enable it automatically after auto calibration is done
          // i.e. readings don't flow anymore, and sum of deltas is low
          // abs() as it can be negative
          if (!isFirstLoop && abs(deltaYaw) < 0.15 && abs(deltaPitch) < 0.15){
            isMouseEnabled = true;
            Serial.println("CALIBRATION SUCCESS");
            digitalWrite(LED_MOUSE, HIGH);
          }
        }
        
        // it waits for MPU6050 anyway, so for default settings
        // any delay below 50 will not have any effect
        // for setRate(2) there is 30ms between each iteration
        // for setRate(3) it's 40
        // setRate(4) is default and results in 50ms delta time
        delay(5);
    }
}

// Arduino function to move mouse takes signed char as params
// singed char range is [-128; 127]
// this range is too small, we need to have our own function
// that would take the bigger numbers and just call the arduino function multiple times
// e.g. for moveMouse(420, -200)
// it will actually call
// Mouse.move(39, -73)
// Mouse.move(127, -127)
// Mouse.move(127, 0)
// Mouse.move(127, 0)
void inline moveMouse(short xValue, short yValue) {
  
  int xValueSign = xValue >= 0 ? 1 : -1;
  int yValueSign = yValue >= 0 ? 1 : -1;
  
  int fullMoveComplementX = xValue % MAX_CHAR_VAL;
  int fullMoveComplementY = yValue % MAX_CHAR_VAL;
  Mouse.move(fullMoveComplementX, fullMoveComplementY);
  
  xValue = xValue - fullMoveComplementX;
  yValue = yValue - fullMoveComplementY;
  
  while (true) {
      // xValue and yValue SHOULD BE divisible by MAX_CHAR_VAL in this place
      // i.e. (a - (a % b)) % b == 0
      if (xValue != 0 && yValue != 0) {
          Mouse.move(MAX_CHAR_VAL*xValueSign, MAX_CHAR_VAL*yValueSign);
          xValue -= MAX_CHAR_VAL*xValueSign;
          yValue -= MAX_CHAR_VAL*yValueSign;
      } else if (xValue != 0 && yValue == 0) {
          Mouse.move(MAX_CHAR_VAL*xValueSign, 0);
          xValue -= MAX_CHAR_VAL*xValueSign;
      } else if (xValue == 0 && yValue != 0) {
          Mouse.move(0, MAX_CHAR_VAL*yValueSign);
          yValue -= MAX_CHAR_VAL*yValueSign;
      } else {
          break;
      }
  }
}

