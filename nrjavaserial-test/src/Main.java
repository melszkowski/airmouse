import gnu.io.NRSerialPort;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        for (String s : NRSerialPort.getAvailableSerialPorts()) {
            System.out.println("Availible port: " + s);
        }
        String port = "COM14";
        int baudRate = 115200;
        NRSerialPort serial = new NRSerialPort(port, baudRate);
        serial.connect();

        DataInputStream ins = new DataInputStream(serial.getInputStream());
        DataOutputStream outs = new DataOutputStream(serial.getOutputStream());

        int b = 195;
        int a = 0;
        try {
            outs.write(b);
            a = ins.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Read: " + a);

        serial.disconnect();
    }
}
