package myApp;

public enum InputMode {
    LETTERS_LATIN,
    DIGITS,
    SPECIAL_CHARACTERS;

    private static InputMode[] vals = values();

    public InputMode next() {
        return vals[(this.ordinal() + 1) % vals.length];
    }

    public static InputMode getDefault() {
        return vals[0];
    }
}
