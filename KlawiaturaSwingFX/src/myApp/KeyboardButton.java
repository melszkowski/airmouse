package myApp;

import javafx.scene.control.Button;

import java.awt.event.KeyEvent;
import java.util.concurrent.atomic.AtomicInteger;

public class KeyboardButton extends Button {
    private static AtomicInteger idCounter = new AtomicInteger(0);
    public final int id;
    private int keyCode;

    public KeyboardButton() {
        super();
        this.id = idCounter.getAndIncrement();
    }

    public KeyboardButton(String text) {
        this(text, KeyEvent.VK_UNDEFINED);
    }

    public KeyboardButton(String text, int keyCode) {
        super(text);
        this.id = idCounter.getAndIncrement();
        this.keyCode = keyCode;
    }

}
