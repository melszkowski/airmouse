package myApp;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javax.naming.ldap.Control;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class Main {

    public static final int KEYBOARD_HEIGHT = 400;
    public static final int KEYBOARD_WIDTH;
    private static final int SYSTEM_BAR_HEIGHT = 40;

    static {
        KEYBOARD_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;

    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Main main = new Main();
                main.initAndShowGUI();
            }
        });
    }

    private void initAndShowGUI() {
        // This method is invoked on the EDT thread
        JFrame frame = new JFrame("Wirtualna klawiatura");
        final JFXPanel fxPanel = new JFXPanel();
        frame.add(fxPanel);

        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        frame.setSize((int) screenSize.getWidth(), KEYBOARD_HEIGHT);
        frame.setLocation(0, (int) (screenSize.getHeight() - KEYBOARD_HEIGHT - SYSTEM_BAR_HEIGHT));

        // TODO: uncomment at the end of development to hide frames
        //frame.setUndecorated(true);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setFocusableWindowState(false);
        frame.setFocusable(false);
        frame.enableInputMethods(false);
        frame.setAlwaysOnTop(true);
        frame.setAutoRequestFocus(false);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.out.println("ZAMYKANIE");
                System.out.println("zamykam port...");
                ArduinoFeedbackSender.getInstance().serial.disconnect();
                Controller.closeFile();
                System.out.println("Zamknalem plik");
            }
        });

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                initFX(fxPanel);
            }
        });
    }

    private void initFX(JFXPanel fxPanel) {
        // This method is invoked on the JavaFX thread
        Scene scene = createScene();
        fxPanel.setScene(scene);
    }

    private Scene createScene() {
        Parent root = null;

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("keyboardView.fxml"));
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Scene scene = new Scene(root);
        return (scene);
    }
}