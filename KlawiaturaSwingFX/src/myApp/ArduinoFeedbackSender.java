package myApp;

import gnu.io.NRSerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.TooManyListenersException;

public class ArduinoFeedbackSender {

    static boolean isDmpInitialized = false;

    private static ArduinoFeedbackSender instance;
    NRSerialPort serial;
    DataInputStream inputStream;
    DataOutputStream outputStream;

    private ArduinoFeedbackSender() {
        initialize();
        System.out.println("ArduinoFeedbackSender initialized");

    }

    public static synchronized ArduinoFeedbackSender getInstance() {
        if (instance == null) {
            instance = new ArduinoFeedbackSender();
        }

        return instance;
    }

    private void initialize() {

        SerialPortEventListener serialPortEventListener = new SerialPortEventListener() {
            @Override
            public void serialEvent(SerialPortEvent ev) {
                handleIncomingData(ev);
            }
        };

        Set<String> availableSerialPorts = NRSerialPort.getAvailableSerialPorts();

        for (String s : availableSerialPorts) {
            System.out.println("Available ports: " + s);
        }

        String port = null;

        try {
            port = availableSerialPorts.iterator().next();
        } catch (Exception e) {
            System.out.println("No devices connected to COM ports, exiting");
            e.printStackTrace();
            System.exit(0);
        }
        int baudRate = 115200;
        serial = new NRSerialPort(port, baudRate);

        if (serial.connect()) {
            System.out.println("Connected to Arduino on port: " + port);
        } else {
            System.out.println("Failed to connect!");
        }

        try {
            serial.addEventListener(serialPortEventListener);
        } catch (TooManyListenersException e) {
            e.printStackTrace();
        }

        try {
            serial.getSerialPortInstance().clearCommInput();
        } catch (UnsupportedCommOperationException e) {
            e.printStackTrace();
        }

        inputStream = new DataInputStream(serial.getInputStream());
        outputStream = new DataOutputStream(serial.getOutputStream());

    }

    private void handleIncomingData(SerialPortEvent ev) {
        if (ev.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                int numOfBytesToRead = inputStream.available();
                //print number of bytes available to read
                //System.out.println("DATA_AVAILABLE - bytes to read: " + numOfBytesToRead);
                byte[] contentReceived = new byte[numOfBytesToRead];

                inputStream.read(contentReceived);
                String contentReceivedAsString = new String(contentReceived);

                System.out.println("RECEIVED: " + contentReceivedAsString);

                // initialize dmp (i.e. send any character) if it not initialized yet
                // if it's already initialized, check if incoming message is a number used for calibration
                if (!isDmpInitialized) {
                    initializeDmp(contentReceivedAsString);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void initializeDmp(String messageReceived) {
        if (messageReceived.contains("Send any")) {
            System.out.println("Sending char... ('a')");
            try {
                char byteToBeWritten = 'a';
                outputStream.write(byteToBeWritten);
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            isDmpInitialized = true;
        }
    }


}