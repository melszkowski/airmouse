package myApp;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;

import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import static java.awt.event.KeyEvent.*;

public class Controller implements Initializable {

    private final String LATIN_ALPHABET_EXTENDED = "ABCDEFGHIJKLMNOPQRSTUVWXYZ⌫⏎␣,.?";
    //private final String LATIN_ALPHABET_EXTENDED_FREQ_SORTED = "ETAOINSHRDLCUMWFGYPBVKJXQZ⌫⏎␣,.?";
    private final String LATIN_ALPHABET_EXTENDED_FREQ_SORTED = "EATIONSRHLDCUMFPGWYBVKXJZQ⌫⏎␣,.?";

    private final String SPECIAL_CHARACTERS = "!@#$%^&*()_+-=[]\\{}|;:'\",<.>/?";
    private final String SPECIAL_CHARACTERS_NEEDING_SHIFT_PRESSED = "!@#$%^&*()_+{}|:\"<>?";

    private final String DIGITS = "0123456789";

    private final int NUMBER_OF_BUTTONS = 4;

    public List<Button> listOfLetterButtons;
    @FXML
    public HBox keysContainer;

    Robot robot;

    InputMode inputMode;
    ArduinoFeedbackSender arduinoFeedbackSender;

    private String logFilename;
    FileWriter fileWriter;
    static PrintWriter printWriter;
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    // this is not a direct keyCode that can be used with awt.robot
    // it returns a code for main button, if shift needs to be pressed simultaneously
    // you can check for it with doesNeedShiftPressed(char c)
    // e.g. for '@' flow would be like:
    // if (doesNeedShiftPressed('@'))
    //   robot.keyPress(VK_SHIFT);
    // robot.keyPress(getKeyCodeFromSpecialCharacter('@'));
    // if (doesNeedShiftPressed('@'))
    //   robot.keyRelease(VK_SHIFT);
    private static int getKeyCodeFromSpecialCharacter(char character) {
        switch (character) {
            case '!':
                return VK_1;
            case '@':
                return VK_2;
            case '#':
                return VK_3;
            case '$':
                return VK_4;
            case '%':
                return VK_5;
            case '^':
                return VK_6;
            case '&':
                return VK_7;
            case '*':
                return VK_8;
            case '(':
                return VK_9;
            case ')':
                return VK_0;
            case '_':
                return VK_MINUS;
            case '+':
                return VK_EQUALS;
            case '-':
                return VK_MINUS;
            case '=':
                return VK_EQUALS;
            case '[':
                return VK_OPEN_BRACKET;
            case ']':
                return VK_CLOSE_BRACKET;
            case '\\':
                return VK_BACK_SLASH;
            case '{':
                return VK_OPEN_BRACKET;
            case '}':
                return VK_CLOSE_BRACKET;
            case '|':
                return VK_BACK_SLASH;
            case ';':
                return VK_SEMICOLON;
            case ':':
                return VK_SEMICOLON;
            case '\'':
                return VK_QUOTE;
            case '"':
                return VK_QUOTE;
            case ',':
                return VK_COMMA;
            case '<':
                return VK_COMMA;
            case '.':
                return VK_PERIOD;
            case '>':
                return VK_PERIOD;
            case '/':
                return VK_SLASH;
            case '?':
                return VK_SLASH;
            default:
                return VK_UNDEFINED;
        }
    }

    private void reloadButtons(int numberOfButtons) {
        keysContainer.getChildren().removeAll();
        listOfLetterButtons.clear();

        for (int i = 1; i <= numberOfButtons; i++) {
            Button button = new Button();
            button.setId("letterButton" + i);
            button.setOnMouseReleased(this::handleLetterButton);
            button.setTextAlignment(TextAlignment.CENTER);
            button.setStyle("-fx-font-size: " + "35.0" + ";");
            button.setMnemonicParsing(false);
            listOfLetterButtons.add(button);
        }

        listOfLetterButtons.forEach(e -> keysContainer.getChildren().add(e));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        listOfLetterButtons = new ArrayList<>();

        reloadButtons(NUMBER_OF_BUTTONS);

        setButtons(LATIN_ALPHABET_EXTENDED);

        try {
            robot = new Robot();
            robot.setAutoDelay(10);
        } catch (AWTException e) {
            e.printStackTrace();
        }

        inputMode = InputMode.getDefault();

        arduinoFeedbackSender = ArduinoFeedbackSender.getInstance();
        System.out.println("ArduinoFeedbackSender created");

        DateFormat doLoguFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

        Date d = new Date();
        logFilename = doLoguFormat.format(d) + "-log.txt";

        fileWriter = null;
        try {
            fileWriter = new FileWriter(logFilename);
        } catch (IOException e) {
            System.out.println("File not found?");
            e.printStackTrace();
        }
        printWriter = new PrintWriter(fileWriter);


    }

    void toggleInputMode() {
        inputMode = inputMode.next();
        setButtons(getCurrentCharSet());
    }

    private int keyCodeFromString(String text) {
        if (text == null || text.length() < 1)
            return VK_UNDEFINED;
        // assume it's a printable character:
        if (LATIN_ALPHABET_EXTENDED.contains(text)) {
            return text.charAt(0);
        } else if (DIGITS.contains(text)) {
            return text.charAt(0);
        } else if (SPECIAL_CHARACTERS.contains(text)) {
            return getKeyCodeFromSpecialCharacter(text.charAt(0));
        }
        return VK_UNDEFINED;
    }

    @FXML
    void handleLetterButton(MouseEvent event) {
        if (event.getButton() == MouseButton.PRIMARY) {
            Button button = (Button) event.getSource();
            String buttonText = button.getText();
            System.out.println("Pressed button: " + buttonText);
            if (buttonText.length() > 1) {
                setButtons(buttonText);
            } else {
                if (buttonText.length() > 0) {
                    String pressed = buttonText;
                    // handle special buttons
                    if (pressed.equals("⏎")) {
                        robot.keyPress(VK_ENTER);
                        robot.keyRelease(VK_ENTER);

                    } else if (pressed.equals("⌫")) {
                        robot.keyPress(VK_BACK_SPACE);
                        robot.keyRelease(VK_BACK_SPACE);
                        Date d = new Date();
                        printWriter.println(dateFormat.format(d) + "\t usunieta literka");
                    } else if (pressed.equals("␣")) {
                        robot.keyPress(VK_SPACE);
                        robot.keyRelease(VK_SPACE);
                    } else if (SPECIAL_CHARACTERS.contains(pressed)) {

                        int mainKeyCode = getKeyCodeFromSpecialCharacter(pressed.charAt(0));
                        boolean needsShiftPressed = doesNeedShiftPressed(pressed.charAt(0));

                        if (needsShiftPressed)
                            robot.keyPress(VK_SHIFT);
                        robot.keyPress(mainKeyCode);
                        robot.keyRelease(mainKeyCode);
                        if (needsShiftPressed)
                            robot.keyRelease(VK_SHIFT);

                    } else {

                        // handle normal letter
                        robot.keyPress(keyCodeFromString(pressed));
                        robot.keyRelease(keyCodeFromString(pressed));

                        Date date = new Date();
                        printWriter.println(dateFormat.format(date) + "\t wcisnieta literka");
                    }
                }

                setButtons(getCurrentCharSet());
            }
        } else if (event.getButton() == MouseButton.SECONDARY) {
            // change input mode after right-mouse-button is clicked
            // i.e. letter -> digits -> special chars etc
            toggleInputMode();
        }

    }

    private boolean doesNeedShiftPressed(char character) {
        return SPECIAL_CHARACTERS_NEEDING_SHIFT_PRESSED.contains(Character.toString(character));
    }

    private String getCurrentCharSet() {
        switch (inputMode) {
            case DIGITS:
                return DIGITS;
            case SPECIAL_CHARACTERS:
                return SPECIAL_CHARACTERS;
            case LETTERS_LATIN:
            default:
                return LATIN_ALPHABET_EXTENDED;
        }
    }

    private void setButtons(String textToBeSplit) {
        int numberOfButtons;

        if (textToBeSplit.length() > listOfLetterButtons.size()) {
            numberOfButtons = listOfLetterButtons.size();
        } else {
            numberOfButtons = textToBeSplit.length();
        }

        // set buttons' text:
        int numberOfLettersInOnePart = textToBeSplit.length() / numberOfButtons;
        int numberOfButtonsWithExtraLetter = textToBeSplit.length() % numberOfButtons;

        int startIndex = 0;
        for (int i = 0; i < numberOfButtons; i++) {

            int endIndex = startIndex + numberOfLettersInOnePart;

            // e.g. for 9-letter string and 4 buttons in 1st round one of buttons has 3 letters, while others have 2
            if (numberOfButtonsWithExtraLetter > 0) {
                endIndex += 1;
                numberOfButtonsWithExtraLetter--;
            }

            endIndex = endIndex > textToBeSplit.length() ? textToBeSplit.length() : endIndex;
            String textToBeSet = textToBeSplit.substring(startIndex, endIndex);
            listOfLetterButtons.get(i).setText(textToBeSet);
            startIndex = endIndex;
        }

        // set button's size:
        for (Button button : listOfLetterButtons) {
            button.setMinWidth(Main.KEYBOARD_WIDTH / (double) numberOfButtons);
            button.setMinHeight(Main.KEYBOARD_HEIGHT);
        }

    }

    public static void closeFile(){
        if(printWriter != null)
            printWriter.close();
    }


}